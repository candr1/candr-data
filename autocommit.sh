#!/bin/bash
cd $(dirname $0)
./dump-sql.sh
git add .
git commit -m "Database backup" --author="CANDR Crontab <gitlab-cron@candr.org.uk>"
GIT_SSH_COMMAND='ssh -i id_ed25519 -o IdentitiesOnly=yes' git push
