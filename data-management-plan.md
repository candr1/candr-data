# University of Glasgow

## Data Management Plan: CANDR

### 1. Overview

- Student name: *Joshua Stutter*
- Supervisor names: *D. McGuinness, J. Tucker, T. Duguid*
- Project title: *A Modern Study of Thirteenth-Century Organa and Motet: The Clausula as Fundamental Unit*
- Funder and award number: *n/a*
- Project summary: *Discovering a tighter set of parameters that define a thirteenth-century clausula:*
  1. *What is a clausula, exactly? Definitions of clausulae are wide-ranging in modern writing.*
  2. *To what extent can the clausula be conceived of as the atom of thirteenth-century polyphony in organa and motet, and what modern methodologies can we apply to this question?*

  *I am conducting a survey of the notational content in the sources of thirteenth-century polyphony. My research methodologies are focused upon:*
  - *Digital humanities*
  - *MEI*
  - *OCR / Machine learning*
  - *Accessible web technology as research output*

### 2. Data

- What types of data will be collected or created: *Location and boundary data of features on manuscript images. Editorial data on the linking of features. Image metadata.*
- What formats will you use: *Database schema (SQL) and JSON, alongside transformations to XML.*
- How much data will you collect?: *In the order of a few thousand records.*

### 3. Documentation

- How will the data be documented and described?: *As an entity relationship diagram. Self-documentating database migrations.*
- Are there any standards for this in your field of research?: *No.*

### 4. Ethics and Intellectual Property

- Who owns the data in your project?:
  - *Myself (self-created data)*
  - *No-one (images placed in the public domain: CC0)*
  - *Libraraies and image repositories (various CC-based licences)*
- Describe any ethical, legal or commercial considerations relating to your research data: *There are some manuscript images that are not available to publish under share-alike licences, only available for private study.*
- How will these concerns be dealt with?: *Those images will be protected from global view in the repository, remaining private to myself. The data that I will create from them (feature tracings) will be transformative (under fair use). The tracings will be made public but the source images will remain hidden.*

### 5. Storage and organisation

- How will the data be named, organised and structured?: *In a database using the schema above. The images are stored on disk.*
- How will the data be stored for the duration of the project?: *In that same database.*
- How will the data be backed up during the project?: *A nightly SQL dump of the database is automatically committed to the repository, so the database plus its entire version history is saved.*
- Does access to the data need to be controlled for the duration of the project?: *To the hidden images, yes.*
- Who has the right to access the data during the project?: *Currently anyone, except the hidden images which are accessible only to myself.*

### 6. Deposit and long-term preservation

- Which data should be retained long-term?: *The resulting database as well as its driving code.*
- How long will data be retained for?: *As long as possible.*
- Where will the data be archived at the end of the project?: *In this repository, as well as a snapshot of the completed database uploaded to Enlighten.*
- What formats will the database be archived in?: *SQL dump*

### 7. Data sharing

- Is any of the data suitable for sharing?: *Yes, except for those hidden images.*
- How will the data be shared?: *Via a publicly-accessible website.*
- Who should be able to access and use the shared data?: *Anyone.*

### 8. Implementation

- Who is responsible for implementing this plan?: *Myself, Joshua Stutter.*
- How will this plan be kept up-to-date?: *Each alteration to the database schema will be considered against this plan, and supervisors if necessary. This document will be updated to match the current data management, and stored alongside the data in the repository.*
- What actions are necessary to implement this plan?: *Approval of this plan, and uploading the plan to the repository.*
- What training or further information is needed to implement this plan?: *None.*
