#!/bin/bash
niceness=19
set -x
git config pack.windowMemory "100m"
git config pack.packSizeLimit "100m"
git config pack.threads "1"
GIT_SSH_COMMAND='ssh -i id_ed25519 -o IdentitiesOnly=yes'
nice -n 19 git remote prune origin
nice -n 19 git repack
nice -n 19 git prune-packed
nice -n 19 git reflog expire --expire=1.week.ago
nice -n 19 git gc --aggressive
