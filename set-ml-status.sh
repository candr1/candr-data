#!/bin/sh
# $1 = candr-ml-runner-host
# $2 = private-key
# $3 = private-key-password
# $4 = status

# import key
export GPG_TTY=$(tty)
fingerprint=$(echo "$2" | gpg --import --pinentry-mode loopback --yes --batch --passphrase "$3" - 2>&1 | grep 'gpg: key ' | awk 'NR==1{print substr($3, 1, length($3)-1)}')
# trust key
expect -c "spawn gpg --edit-key "$fingerprint" trust quit; send \"5\ry\r\"; expect eof" 2>&1 > /dev/null
# get and decrypt challenge
challenge=$(curl -lS "http://${1}/challenge" | \
	jq -r '.challenge')
answer=$(echo "$challenge" | \
	base64 -d | \
	gpg --pinentry-mode loopback --yes --batch --passphrase "$3" --decrypt -)
json=$(jq -n --arg c "$challenge" --arg a "$answer" --arg s "$4" \
	'{"challenge": $c, "answer": $a, "status": $s}')
curl \
	--header 'Content-Type: application/json' \
	--request POST \
	--data "$json" \
	"http://${1}/status"
