Warning to myself: Don't just delete this repository because your SSH deploy keys are in here. You need to:

1. Copy the keys out
2. Re-clone
3. Run `git config core.sshCommand "ssh -o IdentitiesOnly=yes -i ~/.ssh/private-key-filename-for-this-repository -F /dev/null"`
4. If you have cloned via https, change to ssl: `git remote set-url origin ssh://yockyrr@gitlab.com/candr1/candr-data.git`
