#!/bin/bash
pg_dump -h 127.0.0.1 -U candr -d candr -w \
	--exclude-table-data=users \
	--exclude-table-data=machine_learning_challenges \
	--exclude-table-data=machine_learning_statuses > production.sql
split -b 50M production.sql production.sql-part_
rm -f production.sql
