#!/bin/sh
# $1 = host
# $2 = exit status
# $3 = sleep
# $4 = maximum no. of timeouts
timesout=0
while [ "$status" != "$2" ]; do
	resp=$(curl -ls "$1")
	lu=$(echo "$resp" | jq -r '.last_updated')
	status=$(echo "$resp" | jq -r '.status')
	if [ "$status" != "$oldstatus" ]; then
		oldstatus="$status"
		oldlu="$lu"
		timesout=0
		echo "[$(date --utc +%FT%T%Z)] Status: $status"
	else
		echo "[$(date --utc +%FT%T%Z)] Last updated: $lu (${status})"
		if [ "$lu" == "$oldlu" ]; then
			if [ $((++timesout)) -gt $4 ]; then
				echo "Error: Reached maximum timeout"
				exit
			else
				echo "Warning: timeout ${timesout}/${4}"
			fi
		else
			oldlu="$lu"
			timesout=0
		fi
	fi
	sleep "$3"
done
